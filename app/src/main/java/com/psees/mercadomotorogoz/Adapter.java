package com.psees.mercadomotorogoz;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by tb_laota on 9/21/2015.
 */
public class Adapter extends BaseAdapter{
    private LayoutInflater inflater;
    private Activity activity;
    private int id;
    private List<Item> items;
    ImageLoader imageLoader=AppController.getmInstance().getmImageLoader();
    public Adapter(Activity activity, int resource, List<Item> items){
        this.activity=activity;
        this.id=resource;
        this.items=items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView ==null){
            LayoutInflater inflater =activity.getLayoutInflater();
            convertView = inflater.inflate(id, null);
        }
        if(imageLoader==null)
            imageLoader=AppController.getmInstance().getmImageLoader();
        NetworkImageView imageView= (NetworkImageView) convertView.findViewById(R.id.image_view);
        TextView title= (TextView) convertView.findViewById(R.id.tv_title);
        TextView rate= (TextView) convertView.findViewById(R.id.tv_rate);

        final TextView year= (TextView) convertView.findViewById(R.id.tv_year);
        TextView quantity= (TextView) convertView.findViewById(R.id.cantidad);
        final TextView subtotal= (TextView) convertView.findViewById(R.id.textView3);
        final TextView subtotalTv= (TextView) convertView.findViewById(R.id.subtotal);

        Button delButton= (Button) convertView.findViewById(R.id.button_delete);
        final EditText cantidad= (EditText) convertView.findViewById(R.id.editText);
        //getting data for row
        final Item item=items.get(position);
        imageView.setImageUrl(item.getImage(), imageLoader);
        //title
        title.setText(item.getTitle());
        //rate
        rate.setText(String.valueOf(item.getRate()));

        //year
        year.setText("$ "+String.valueOf(item.getYear()));
        quantity.setText("Cantidad");
        subtotalTv.setText("Subtotal $");
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(item);
                notifyDataSetChanged();
            }
        });

        cantidad.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction()==KeyEvent.ACTION_DOWN) && (keyCode==KeyEvent.KEYCODE_ENTER)){
                    double cantida=Double.parseDouble(cantidad.getText().toString());
                    String price=item.getYear();
                    double precio=Double.parseDouble(price);
                    item.setSubtotal(cantida*precio);
                    subtotal.setText(item.getSubtotal().toString());

                    item.setQuantity(Integer.parseInt(cantidad.getText().toString()));
                    //notifyDataSetChanged();
                    return true;
                }

                return false;
            }
        });

        return convertView;
    }
}