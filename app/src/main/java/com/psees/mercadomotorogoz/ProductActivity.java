package com.psees.mercadomotorogoz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;

public class ProductActivity extends AppCompatActivity {

    TextView tvProductName;
    ProgressDialog progressDialog;
    RatingBar actualProductRating;
    ImageView ivProductImage;
    TextView tvProductPrice;
    TextView tvProductDescription;
    TextView tvProductStock;
    Button buttonValuation;
    RatingBar ratingBarScore;
    EditText etCommentary;
    Button buttonSend;
    ConstraintLayout mainProductLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        tvProductName = (TextView)findViewById(R.id.tvProductName);
        progressDialog = new ProgressDialog(ProductActivity.this);
        ivProductImage = (ImageView) findViewById(R.id.ivProductImage);
        actualProductRating = (RatingBar) findViewById(R.id.actualProductRating);
        tvProductPrice = (TextView) findViewById(R.id.tvProductPrice);
        tvProductDescription = (TextView) findViewById(R.id.tvProductDescription);
        tvProductStock = (TextView) findViewById(R.id.tvProductStock);
        buttonValuation = (Button) findViewById(R.id.buttonValuation);
        ratingBarScore = (RatingBar) findViewById(R.id.ratingBarScore);
        etCommentary = (EditText) findViewById(R.id.etCommentary);
        buttonSend=(Button) findViewById(R.id.buttonSend);
        mainProductLayout = (ConstraintLayout)findViewById(R.id.MainProductLayout);

        final String productId=getIntent().getExtras().getString("ids");
        new ProductAsync().execute(productId); //Debe enviarse el id del producto para procesarse en doinBackground
        new ValuationAsync().execute("5","112");//Debe enviarse el id del producto y order_detail(id)

        buttonValuation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent valuationActivityIntent = new Intent(getApplicationContext(), ValuationActivity.class);
                valuationActivityIntent.putExtra("productId",productId);
                startActivity(valuationActivityIntent);
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String score = Float.toString(ratingBarScore.getRating()*2); // Se extrae el valor calificado en la barra
                String commentary = etCommentary.getText().toString();// Se extrae el comentario escrito
                new SendScore().execute("5","112",score);//Debe enviarse productId,OrderDetail(ID) y Score en este orden
                new SendCommentary().execute("5","112",commentary); //Debe enviarse productId, OrderDetail(ID) y Commentary en este orden

            }
        });


    }

    public class ProductAsync extends AsyncTask<String, Void, String> {

        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";



        @Override
        protected String doInBackground(String... params) {
            //Id del producto solicitado
            String productId =params[0];
            //URL donde se encuentra la API productos.
            String productsUrl = "https://pseesapiproducts.herokuapp.com/products/"+productId;

            try
            {
                URL url = new URL(productsUrl);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {

                    Log.e("App", "En background 6");
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background e", ex);
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
           mainProductLayout.setVisibility(View.GONE);
            //Se establecen los parametros del dialogo de proceso tipo,titulo,mensaje y si es posible cancelarlo.
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle("Procesando");
            progressDialog.setMessage("Por favor espere....");
            progressDialog.setCancelable(false);
            progressDialog.show();


        }

        @Override
        protected void onPostExecute(String result) {

            String productName = ""; //Variable donde se almacena el nombre del producto recuperado
            float productRating = 0.0f; // Variable donde se almacena el puntaje del producto
            String urlProductImage = "http:"; //Variable donde se almacena la url de la imagen del producto
            String productPrice ="$ "; //Variable que contiene el precio del producto
            String productStock ="";// Variable que contiene la cantidad de stock del producto
            String productDescription= ""; // Variable que almacena la descripcion del producto


            Toast.makeText(getBaseContext(), "Recibido!", Toast.LENGTH_LONG).show();

            //Ajustes de rating bar
            actualProductRating.setIsIndicator(true);
            actualProductRating.setNumStars(5);
            actualProductRating.setMax(10);
            actualProductRating.setStepSize(0.5f);


            try {
                JSONObject jsonObject = new JSONObject(result);
                productName = jsonObject.getString("product_name");
                tvProductName.setText(productName); //Se actualiza el nombre del producto
                urlProductImage=urlProductImage+jsonObject.getString("image");
                Picasso.with(ProductActivity.this).load(urlProductImage).into(ivProductImage);
                productRating=Float.parseFloat(jsonObject.getString("score"));
                actualProductRating.setRating((productRating/10)*5);// Se actualiza el número de estrellas
                productPrice=productPrice+jsonObject.getString("price");
                tvProductPrice.setText(productPrice); // Se actualiza el precio del producto
                productDescription= jsonObject.getString("description");
                tvProductDescription.setText(productDescription); // Se actualiza la descripcion del producto
                productStock=jsonObject.getString("stock");
                tvProductStock.setText(productStock);//Se actualiza el stock del producto

                progressDialog.dismiss();
                mainProductLayout.setVisibility(View.VISIBLE);
                Toast toast2 = Toast.makeText(getApplicationContext(), productName, Toast.LENGTH_SHORT);
                toast2.show();
            } catch (JSONException e) {
                e.printStackTrace();

            }



        }


    }

    public class ValuationAsync extends AsyncTask<String, Void, String> {
        //Id del producto solicitado
        String productId;
        String order_detail;
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";



        @Override
        protected String doInBackground(String... params) {
            //URL donde se encuentra la API valuation.
            productId=params[0];
            order_detail=params[1];

            String productsUrl = "https://pseesapivaluations.herokuapp.com/values/"+productId+".json";
            try
            {
                URL url = new URL(productsUrl);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {

                    Log.e("App", "En background 6");
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background e", ex);
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
        /*
            //Se establecen los parametros del dialogo de proceso tipo,titulo,mensaje y si es posible cancelarlo.
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle("Procesando");
            progressDialog.setMessage("Por favor espere....");
            progressDialog.setCancelable(false);
            progressDialog.show();

        */

        }

        @Override
        protected void onPostExecute(String result) {

            //Ajustes de rating bar
            ratingBarScore.setIsIndicator(false);
            ratingBarScore.setNumStars(5);
            ratingBarScore.setMax(10);
            ratingBarScore.setStepSize(0.5f);

            try {
                JSONArray jsonArray = new JSONArray(result);
                for(int i =0;i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if(order_detail.equals(jsonObject.getString("order_detail")))
                    {


                        //Si score es nulo y commentary es nulo
                        if(jsonObject.getString("score")=="null"&&jsonObject.getString("commentary")=="null")
                        {

                            ratingBarScore.setRating(0.0f);
                            etCommentary.setText("");

                        }
                        //Si score es diferente de nulo y commentary nulo
                        if(jsonObject.getString("score")!="null"&&jsonObject.getString("commentary")=="null")
                        {
                            ratingBarScore.setRating((Float.parseFloat(jsonObject.getString("score"))/10)*5);
                            etCommentary.setText("");

                        }
                        if(jsonObject.getString("score")=="null"&&jsonObject.getString("commentary")!="null")
                        {
                            ratingBarScore.setRating(0.0f);
                            etCommentary.setText(jsonObject.getString("commentary"));

                        }
                        if(jsonObject.getString("score")!="null"&&jsonObject.getString("commentary")!="null")
                        {
                            ratingBarScore.setRating((Float.parseFloat(jsonObject.getString("score"))/10)*5);
                            etCommentary.setText(jsonObject.getString("commentary"));
                        }

                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        }


    }

    public class SendScore extends AsyncTask<String, Void, String>{

        String urlUpdateScore = "https://pseesapivaluations.herokuapp.com//valuations/score_update/";



        @Override
        protected String doInBackground(String... params) {

            try{
                URL url = new URL(urlUpdateScore);
                JSONObject scoreData = new JSONObject();
                scoreData.put("product_id",params[0]); //Se envia el id del producto
                scoreData.put("id",params[1]); //Se envia el id de order_detail
                scoreData.put("score",params[2]); // Se envia la nueva calificacion
                Log.e("params",scoreData.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("PUT");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(scoreData));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }

            }

            catch (Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }
    }

    public class SendCommentary extends AsyncTask<String, Void, String>{

        String urlUpdateScore = "https://pseesapivaluations.herokuapp.com//valuations/commentary_update/";



        @Override
        protected String doInBackground(String... params) {

            try{
                URL url = new URL(urlUpdateScore);
                JSONObject scoreData = new JSONObject();
                scoreData.put("product_id",params[0]); //Se envia el id del producto
                scoreData.put("id",params[1]); //Se envia el id de order_detail
                scoreData.put("commentary",params[2]); // Se envia la nueva calificacion
                Log.e("params",scoreData.toString()); // Se envia el comentario

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("PUT");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(scoreData));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }

            }

            catch (Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


}
