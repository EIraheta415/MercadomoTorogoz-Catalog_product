package com.psees.mercadomotorogoz;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Propieatrio on 18/6/2017.
 */

public class AdapterOrders extends BaseAdapter {
    private LayoutInflater inflater;
    private Activity activityOrders;
    private List<ItemOrders> orders;
    private ProgressDialog dialog;
    private Context mContext = null;
    private  String msg="";
    private List<ItemOrders> array = new ArrayList<ItemOrders>();
    private AdapterOrders adapter;
    public AdapterOrders(Activity activity,List<ItemOrders> items, Context context){
        this.activityOrders=activity;
        this.orders=items;
        this.mContext = context;
    }
    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater) activityOrders.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView ==null){
            convertView=inflater.inflate(R.layout.custom_layout_orders,null);
        }
        final ItemOrders item=orders.get(position);
        Button viewButton= (Button) convertView.findViewById(R.id.button);
        TextView date= (TextView) convertView.findViewById(R.id.textView4);
        TextView total= (TextView) convertView.findViewById(R.id.textView5);
        TextView status= (TextView) convertView.findViewById(R.id.textView6);
        date.setText(item.getDate().substring(0,10));
        total.setText(item.getAmount());
        status.setText(item.getStatus());

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(mContext, "Loading","Please wait..");
                //Creat volley request obj
                String id= String.valueOf(item.getId());
                String url = "https://pseesapipedidos.herokuapp.com/order_details_product/order/"+id;
                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response) {
                                // display response
                                hideDialog();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response;
                                try {
                                    JSONArray jsonDataset1 = jsonObject.getJSONArray("product_name_list");
                                    for (int i=0;i<jsonDataset1.length();i++){
                                        msg+=jsonDataset1.get(i).toString()+" ";

                                    }
                                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                                    alertDialog.setMessage(msg);
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int wich) {
                                                    dialog.dismiss();
                                                }
                                            }
                                    );
                                    alertDialog.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error.Response", error.toString());
                            }
                        }
                );

// add it to the RequestQueue

                AppController.getmInstance().addToRequesQueue(getRequest);
            }
        });

        return convertView;
    }
    public void hideDialog(){

        if(dialog !=null){
            dialog.dismiss();
            dialog=null;
        }
    }
}
