package com.psees.mercadomotorogoz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MainActivityMercadomo extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    boolean executeOnResume;
    TextView name;
    TextView email;
    NavigationView view_n;
    private ListView lista = null;
    private ArrayList<Product> arrayProd = null;
    private AdaptadorProducto adapter = null;
    private ImageView imagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        executeOnResume = false;
        //Here we will check if the token is in shared preferences
        //If it is we will check it with the service if it is still valid
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0); // 0 - for private mode

        //Testing purposes
        /*final SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();*/
        //Recuperando datos
        String tokenRetrieved = pref.getString("authToken", null);

        if(TextUtils.isEmpty(tokenRetrieved)){
            Toast errTok = Toast.makeText(this, "No token saved", Toast.LENGTH_SHORT);
            errTok.show();
            Intent intent = new Intent(MainActivityMercadomo.this, Inicio.class);
            startActivity(intent);


        } else {
            validateToken();
        }


        //Aqui ya se comprobo y se sigue la logica normal, si se quieren extraer se usa shared preferences

        String first_name = pref.getString("first_name", null);
        String mail=pref.getString("email",null);
        Toast showName = Toast.makeText(MainActivityMercadomo.this,"Bienvenido a la tienda: "+ first_name, Toast.LENGTH_LONG);
        showName.show();




        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_mercadomo);
        getSupportActionBar().hide();
    view_n=(NavigationView)findViewById(R.id.nav_view);
        View drawerHeader=view_n.inflateHeaderView(R.layout.nav_header_main_activity_mercadomo);
        name=(TextView)drawerHeader.findViewById(R.id.name);
        email=(TextView)drawerHeader.findViewById(R.id.email);
        name.setText(first_name);
        email.setText(mail);
Menu menu=view_n.getMenu();
        MenuItem outlog=menu.findItem(R.id.outLogin);

        outlog.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //Log.e("Exito","Puedes manejar ya el menu");
                pref.edit().clear();
                Intent intent=new Intent(MainActivityMercadomo.this,Inicio.class);
                startActivity(intent);
                Toast showName = Toast.makeText(MainActivityMercadomo.this,"Esta saliendo de la aplicación", Toast.LENGTH_LONG);
                showName.show();
                return false;
            }

        });

        lista = (ListView)findViewById(R.id.listView);
        arrayProd = new ArrayList<>();
        new listarProductos().execute();
                                                        ////Aca modifica Jaime
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent productIntent = new Intent(getApplicationContext(), ProductActivity.class);

                TextView v=(TextView) view.findViewById(R.id.textView16);
                String bus = v.getText().toString();
                //int ids = Integer.parseInt(bus);

                productIntent.putExtra("ids", bus);
                startActivity(productIntent);
}
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivityMercadomo.this, MainActivityMercadomoOrders.class);
                startActivity(i);
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);




    }


    public class listarProductos extends AsyncTask<Void, Void, String> {

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
        Integer userId = pref.getInt("id", 0);

        String str="https://pseesapiproducts.herokuapp.com/catalog/" + userId;
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String id = jsonobject.getString("id");
                    int ids = Integer.parseInt(id);
                    Log.e(id, "Valor enviado");
                    String nombre = jsonobject.getString("product_name");
                    String cont = jsonobject.getString("description");
                    double prec = Double.parseDouble(jsonobject.getString("price"));
                    String base = jsonobject.getString("image");
                    Log.e(base,"Miremos la img");
                    String conca = "http:" + base;
                    Log.e(conca,"Concatenada");

                    arrayProd.add(new Product(ids, conca,nombre,cont,prec));
                    addList();

                    Log.e("App", nombre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }
    private void addList(){

        adapter = new AdaptadorProducto(this, arrayProd);
        lista.setAdapter(adapter);
    }



    protected void onResume(){
        super.onResume();
        if(executeOnResume){
            validateToken();
        } else {
            executeOnResume = true;
        }

    }


    protected void validateToken(){

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0); // 0 - for private mode
        //Testing purposes
        final SharedPreferences.Editor editor = pref.edit();
        String tokenRetrieved = pref.getString("authToken", null);
        Ion.with(MainActivityMercadomo.this)
                .load("GET", "http://pseesapiauth.herokuapp.com/check?token="+tokenRetrieved)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if(e == null){
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(result);
                                String error = obj.optString("error");

                                if(TextUtils.isEmpty(error)){
                                    Integer id = obj.optInt("id");
                                    String email = obj.optString("email");
                                    String firstName = obj.optString("first_name");
                                    String lastName = obj.optString("last_name");
                                    String dui = obj.optString("dui");
                                    String municipality = obj.optString("municipality");
                                    JSONArray permjson = obj.getJSONArray("permissions");
                                    String[] permstring = permjson.toString().replace("},{", " ,").split(" ");
                                    Set<String> permissions = new HashSet<>(Arrays.asList(permstring));
                                    editor.putInt("id", id);
                                    editor.putString("email", email);
                                    editor.putStringSet("permissions", permissions);
                                    editor.putString("first_name", firstName);
                                    editor.putString("last_name", lastName);
                                    editor.putString("dui", dui);
                                    editor.putString("municipality", municipality);
                                    editor.commit();


                                } else {
                                    //Handle error
                                    Toast err = Toast.makeText(MainActivityMercadomo.this, "Expired", Toast.LENGTH_SHORT);
                                    err.show();
                                    editor.remove("authToken");
                                    editor.commit();
                                    Intent intent = new Intent(MainActivityMercadomo.this, Inicio.class);
                                    startActivity(intent);
                                    finish();
                                }


                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_mercadomo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
