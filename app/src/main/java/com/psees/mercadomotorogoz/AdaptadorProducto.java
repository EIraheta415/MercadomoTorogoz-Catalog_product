package com.psees.mercadomotorogoz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/**
 * Created by mata on 16/6/2017.
 */

public class AdaptadorProducto  extends BaseAdapter{
    private ArrayList<Product> arrayListProd;
    private Context context;
    private LayoutInflater layoutInflater;
    public static int id;
    public int captura;

    public AdaptadorProducto(Context context, ArrayList arrayListProd) {
        this.arrayListProd = arrayListProd;
        this.context = context;
    }


    @Override
    public int getCount() {
        return arrayListProd.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListProd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vista = layoutInflater.inflate(R.layout.layout_producto, parent ,false);
        ImageView img = (ImageView) vista.findViewById(R.id.imageView6);
        TextView nombre = (TextView) vista.findViewById(R.id.textView10);
        TextView desc = (TextView) vista.findViewById(R.id.textView9);
        TextView prec = (TextView) vista.findViewById(R.id.textView8);
        TextView ide = (TextView) vista.findViewById(R.id.textView16);

        //img.setImageBitmap(arrayListProd.get(position).getImage());
        Picasso.with(context).load(arrayListProd.get(position).getImage()).into(img);
        nombre.setText(arrayListProd.get(position).getTitulo());
        desc.setText(arrayListProd.get(position).getContenido());
        prec.setText(String.valueOf(arrayListProd.get(position).getPrecio()));
        ide.setText(String.valueOf(arrayListProd.get(position).getId()));


      /*  img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductActivity.class);
                id = arrayListProd.get(captura).getId();
                    i.putExtra("ids", id);
                    String impresion = String.valueOf(id);
                    Log.e(impresion, "Pasando por el Adaptador");
                    context.startActivity(i);
            }
        });*/

        return vista;
    }
}
