package com.psees.mercadomotorogoz;

import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;


import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;


import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.List;


public class MainActivityMercadomoOrders extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String url="https://pseesapiproducts.herokuapp.com/catalog/1";

    private ProgressDialog dialog;
    private List<Item> array ;
    private List<Item> json;
    private ListView listView;
    private Adapter adapter;
    private EditText quantity;
    private String fecha;
    private String amount;
    private double total=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mercadomo_orders);
        quantity= (EditText) findViewById(R.id.editText);
        listView = (ListView) findViewById(R.id.list_item);
        json = new ArrayList<Item>();



        dialog= new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        //making the request
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                hideDialog();
                //parsing json
                for (int i=0; i<response.length();i++){
                    try{
                        JSONObject obj=response.getJSONObject(i);
                        Item item=new Item();
                        item.setId(obj.getInt("id"));
                        item.setTitle(obj.getString("product_name"));
                        item.setImage(obj.getString("image"));
                        item.setRate(obj.getString("description"));
                        item.setYear(obj.getString("price"));
                        item.setQuantity(0);
                        json.add(item);
                    }catch (JSONException ex){
                        ex.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        });
        array = new ArrayList<Item>();
        listView.setAdapter(adapter);
        adapter = new Adapter(MainActivityMercadomoOrders.this, R.layout.custom_layout, array);
        adapter.notifyDataSetChanged();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String[] items = new String[json.size()];
                final ArrayList selectedItems = new ArrayList();

                for(int i = 0; i < json.size(); i++)
                {
                    items[i] = json.get(i).getTitle().toString();
                }
                AlertDialog dialog = new AlertDialog.Builder(MainActivityMercadomoOrders.this)
                        .setTitle("Seleccione productos a comprar")
                        .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {

                                if (isChecked) {
                                    //Almacena el checkbox seleccionado
                                    selectedItems.add(indexSelected);
                                } else if (selectedItems.contains(indexSelected)) {
                                    //Si estaba seleccionado lo qutia del arreglo
                                    selectedItems.remove(Integer.valueOf(indexSelected));
                                }
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                for(int j = 0; j < selectedItems.size(); j++) {
                                    Item item=new Item();
                                    item.setId(json.get((Integer) selectedItems.get(j)).getId());
                                    item.setTitle(json.get((Integer) selectedItems.get(j)).getTitle().toString());
                                    item.image=(json.get((Integer) selectedItems.get(j)).getImage().toString());
                                    item.setRate(json.get((Integer) selectedItems.get(j)).getRate().toString());
                                    item.setYear(json.get((Integer) selectedItems.get(j)).getYear().toString());
                                    item.setQuantity(0);
                                    array.add(item);

                                }

                                listView.setAdapter(adapter);
                                adapter = new Adapter(MainActivityMercadomoOrders.this, R.layout.custom_layout, array);
                                adapter.notifyDataSetChanged();

                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // Si le da en cancelar asumo que no pasa nada
                            }
                        }).create();
                dialog.show();
            }

        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        AppController.getmInstance().addToRequesQueue(jsonArrayRequest);

    }



    public void hideDialog(){
        if (dialog!=null){
            dialog.dismiss();
            dialog=null;
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_mercadomo_orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id== R.id.action_send){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Introduzca la fecha de entrega del pedido");

// Set up the input
            final EditText input = new EditText(MainActivityMercadomoOrders.this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_DATETIME);
            builder.setView(input);

// Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fecha = input.getText().toString();
                    Toast toast = Toast.makeText(getApplicationContext(), "Enviando...", Toast.LENGTH_LONG);
                    toast.show();

                    String paramIds="[";
                    String paramQuatities="[";
                    ArrayList<String> jsonProductList = new ArrayList<>();
                    ArrayList<String> jsonQuantityList = new ArrayList<>();
                    JSONArray  jsonObject = new JSONArray ();
                    for (int i=0; i<array.size(); i++){
                        Integer ids=(array.get(i)).getId();
                        jsonProductList.add(String.valueOf(ids));
                        Integer quantity=(array.get(i)).getQuantity();
                        jsonQuantityList.add(String.valueOf(quantity));
                        paramIds+=ids.toString()+",";
                        paramQuatities+=quantity.toString()+",";
                    }
                    paramIds.substring(0, paramIds.length()-1);
                    paramQuatities.substring(0, paramQuatities.length()-1);
                    paramIds+="]";
                    paramQuatities+="]";

                    String url2="https://pseesapipedidos.herokuapp.com/orders?client_id=1&product_list="+paramIds+"&quantity_list="+paramQuatities+"&date_delivery='"+fecha +"'";
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url2, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("Response: ", response.toString());
                            total=0;
                            try {
                                JSONArray jsonArray = response;

                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jresponse =
                                            jsonArray.getJSONObject(i);

                                    amount = jresponse.getString("amount");
                                    total+= Double.parseDouble(amount);
                                    Log.d("amount",String.valueOf(total));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivityMercadomoOrders.this).create();
                            alertDialog.setTitle("¡Listo! :) Gracias por Preferirnos");

                            String msg = "La cantidad a pagar es: $"+total;
                            msg+="\n"+"La fecha en que se entregara el pedido es: "+fecha;
                            alertDialog.setMessage(msg);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int wich) {
                                            dialog.dismiss();
                                        }
                                    }
                            );
                            alertDialog.show();
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub

                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(jsonArrayRequest);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();



        }
        if (id==R.id.action_viewOrders){
            Intent i = new Intent(MainActivityMercadomoOrders.this, ViewOrdersActivity.class);
            startActivity(i);
        }


        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
