package com.psees.mercadomotorogoz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistryActitity extends AppCompatActivity {
    private  EditText email;
    private EditText f_name;
    private EditText l_name;
    private EditText password;
    private EditText confirm_password;
    private EditText municipio_t;
    private EditText Dui;
    private Button btn_confirm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry_actitity);
        email=(EditText)findViewById(R.id.email);
        l_name=(EditText)findViewById(R.id.l_name);
        f_name=(EditText)findViewById(R.id.f_name);
        password=(EditText)findViewById(R.id.password);
        confirm_password=(EditText)findViewById(R.id.confirm_password);
        municipio_t=(EditText)findViewById(R.id.municipio);
        Dui=(EditText)findViewById(R.id.Dui);
        btn_confirm=(Button)findViewById(R.id.btn_accept);

        btn_confirm.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                String name= f_name.getText().toString();
                final String last_name=l_name.getText().toString();
                String pass=password.getText().toString();
                String confirm_pass=confirm_password.getText().toString();
                String mail=email.getText().toString();
                String dui=Dui.getText().toString();
                String municipio=municipio_t.getText().toString();
                Ion.with(RegistryActitity.this)
                        .load("POST","https://pseesapiproducts.herokuapp.com/participants")
                        .setBodyParameter("first_name",name)
                        .setBodyParameter("last_name",last_name)
                        .setBodyParameter("email",mail)
                        .setBodyParameter("password",pass)
                        .setBodyParameter("password_confirmation",confirm_pass)
                        .setBodyParameter("product_catalog_id", String.valueOf(2))
                        .setBodyParameter("dui",dui)
                        .setBodyParameter("municipality",municipio)
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {

                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                Log.e("Cabezera", String.valueOf(result.getHeaders().code()));
                                Log.e("Resultado",result.getRequest().toString());
                                Toast err = Toast.makeText(RegistryActitity.this, "Cuenta de "+last_name +" por confirmar en correo"+email, Toast.LENGTH_SHORT);
                                err.show();
                            }
                        });
            }
        });
    }
}
